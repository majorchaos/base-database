# base-Database


## Table of Contents 

* [Name](#Name)
* [Description](#Description)
* [Visuals](#visuals)
* [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Project Status](#project-status)


## Name
base-Database

## Description
follow along tutorial Tech with Tim. 
https://youtu.be/dam0GPOAvVI

Python Flask website with database and user login and notes for each user.

Expanded to include database of links to videos and websites 
useful in learning python and linux.

## Visuals
![image](Links_database_Screenshot.png "application screenshot")

## Installation
Python Flask website requires appropriate server

## Usage
Search through database of educational links of value to python and linux learners

## Roadmap
The links table in the database should have all CRUD capabilities for authorized user(s) only.
Search should be done at the database level instead of HTML level.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
GPLv3

![GitLab](https://img.shields.io/gitlab/license/macksm3/base-Database?style=plastic&color=blue)

## Project status
Using Diane's links spreadsheet as starting point, it is saved as CSV 
and imported as a new table in site database.
The records are all read into html table, than sort and search functions 
are implemented with dataTables.js code.